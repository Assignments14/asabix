<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        Schema::create('post_translations', static function (Blueprint $table) {
            $table->id();
            $table->foreignId('post_id');
            $table->foreignId('language_id');
            $table->string('title');
            $table->string('description')->nullable();
            $table->text('content');

            $table->unique(['post_id', 'language_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(): void
    {
        Schema::dropIfExists('post_translations');
    }
};
