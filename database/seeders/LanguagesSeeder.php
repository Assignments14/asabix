<?php

namespace Database\Seeders;

use Carbon\Carbon;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

final class LanguagesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(): void
    {
        DB::table('languages')->insert([
            'locale' => 'en',
            'prefix' => 'EN',
            'created_at' => Carbon::now(),
        ]);
        DB::table('languages')->insert([
            'locale' => 'ru',
            'prefix' => 'RU',
            'created_at' => Carbon::now(),
        ]);
        DB::table('languages')->insert([
            'locale' => 'ua',
            'prefix' => 'UA',
            'created_at' => Carbon::now(),
        ]);
    }
}
