<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

final class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run(): void
    {
        $this->call([LanguagesSeeder::class]);
        $this->call([TagsSeeder::class]);
        $this->call([PostsSeeder::class]);
    }
}
