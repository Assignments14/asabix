<?php

namespace Database\Seeders;

use Carbon\Carbon;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

final class TagsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(): void
    {
        DB::table('tags')->insert([
            'name' => 'hot',
            'created_at' => Carbon::now(),
        ]);
        DB::table('tags')->insert([
            'name' => 'top',
            'created_at' => Carbon::now(),
        ]);
        DB::table('tags')->insert([
            'name' => 'science',
            'created_at' => Carbon::now(),
        ]);
        DB::table('tags')->insert([
            'name' => 'history',
            'created_at' => Carbon::now(),
        ]);
    }
}
