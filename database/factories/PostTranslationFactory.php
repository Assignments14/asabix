<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\PostTranslation>
 */
final class PostTranslationFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'title' => $this->faker->realText(),
            'description' => $this->faker->realText(),
            'content' => $this->faker->paragraph(),
            'language_id' => $this->faker->randomElement([1, 2, 3]),
        ];
    }
}
