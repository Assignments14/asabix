<?php

use App\Http\Controllers\Api\PostController;
use App\Http\Controllers\Api\TagController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::apiResource('tags', TagController::class);

Route::prefix('posts/{lang_prefix}')->controller(PostController::class)->group(function () {
    Route::get('/', 'index');
    Route::get('/{post_id}', 'show');
    Route::post('/', 'store');
    Route::put('/{post_id}', 'update');
    Route::delete('/{post_id}', 'destroy');
    Route::post('/tags/{post_id}', 'setTags');
});
