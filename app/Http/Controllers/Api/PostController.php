<?php

namespace App\Http\Controllers\Api;

use App\Contracts\PostRepositoryInterface;
use App\Http\Controllers\Controller;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

final class PostController extends Controller
{
    private PostRepositoryInterface $postRepository;

    public function __construct(PostRepositoryInterface $postRepository)
    {
        $this->postRepository = $postRepository;
    }

    public function index(string $lang_prefix): JsonResponse
    {
       $posts = $this->postRepository->getAllByLang($lang_prefix);
       return response()->json(['data' => $posts]);
    }

    public function show(string $lang_prefix, int $post_id): JsonResponse
    {
        return response()->json([
            'data' => $this->postRepository->postById($lang_prefix, $post_id)
        ]);
    }

    public function store(string $lang_prefix, Request $request): JsonResponse
    {
        $postDetails = $request->only([
            'post_id', 'title', 'description', 'content'
        ]);

        return response()->json(
            [
                'data' => $this->postRepository->insertOrUpdate($lang_prefix, $postDetails)
            ],
            Response::HTTP_CREATED
        );
    }

    public function update(string $lang_prefix, int $post_id, Request $request): JsonResponse
    {
        $postDetails = $request->only([
            'title', 'description', 'content'
        ]);

        $postDetails['post_id'] = $post_id;

        return response()->json(
            [
                'data' => $this->postRepository->insertOrUpdate($lang_prefix, $postDetails)
            ]
        );
    }

    public function destroy(string $lang_prefix, int $post_id): JsonResponse
    {
        $this->postRepository->deletePost($lang_prefix, $post_id);
        return response()->json(null, Response::HTTP_NO_CONTENT);
    }

    public function setTags(string $lang_prefix, int $post_id, Request $request): JsonResponse
    {
        $tags = $request->only(['tags']);

        return response()->json([
            'data' => $this->postRepository->setTags($lang_prefix, $post_id, $tags['tags']),
        ], Response::HTTP_CREATED);
    }
}