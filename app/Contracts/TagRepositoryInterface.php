<?php

namespace App\Contracts;

interface TagRepositoryInterface
{
    public function getAllTags();
    public function createTag(array $tagDetails);
    public function tagById(int $id);
    public function updateTag(int $tagId, array $newDetails);
    public function deleteTag(int $tagId);
}