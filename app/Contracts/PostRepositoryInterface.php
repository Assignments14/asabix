<?php

namespace App\Contracts;

use App\Models\PostTranslation;
use Illuminate\Database\Eloquent\Collection;

interface PostRepositoryInterface
{
    public function getAllByLang(string $lang_prefix);

    public function postById(string $lang_prefix, int $post_id): Collection;

    public function insertOrUpdate(string $lang_prefix, array $details): PostTranslation;

    public function deletePost(string $lang_prefix, int $post_id): void;

    public function setTags(string $lang_prefix, int $post_id, array $tags): array;
}