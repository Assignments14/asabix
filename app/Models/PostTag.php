<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

final class PostTag extends Model
{
    use HasFactory;

    public $timestamps = false;

    protected $fillable = ['post_id', 'tag_id'];

    public function post()
    {
        return $this->belongsTo(Post::class);
    }

    public function tag()
    {
        return $this->belongsTo(Tag::class);
    }

    public function setTags(int $post_id, array $data): array
    {
        self::wherePostId($post_id)->delete();

        $bulk_insert = [];
        foreach ($data as $name) {
            $tag = Tag::whereName($name)->first();
            if ($tag) {
                $tag_id = $tag->id;
            } else {
                $tag_id = Tag::create(compact('name'))->id;
            }
            $bulk_insert[] = compact('post_id', 'tag_id');
        }
        self::insert($bulk_insert);

        return $data;
    }
}
