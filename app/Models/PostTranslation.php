<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

final class PostTranslation extends Model
{
    use HasFactory;

    protected $fillable = ['post_id', 'language_id', 'title', 'description', 'content'];

    public $timestamps = false;

    public function language()
    {
        return $this->hasOne(Language::class);
    }

    public function post()
    {
        return $this->belongsTo(Post::class);
    }
}
