<?php

namespace App\Repositories;

use App\Contracts\PostRepositoryInterface;
use App\Models\Language;
use App\Models\Post;
use App\Models\PostTag;
use App\Models\PostTranslation;
use App\Models\Tag;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Arr;

final class PostRepository implements PostRepositoryInterface
{
    private function langIdFromPrefix(string $lang_prefix): int
    {
        return Language::wherePrefix($lang_prefix)->first()->id;
    }

    private function translationsByLangPrefix(string $lang_prefix)
    {
        return PostTranslation::with('post')
            ->whereLanguageId($this->langIdFromPrefix($lang_prefix))
            ->paginate();
    }

    public function getAllByLang(string $lang_prefix)
    {
        return $this->translationsByLangPrefix($lang_prefix);
    }

    public function postById(string $lang_prefix, int $post_id): Collection
    {
        Post::findOrFail($post_id);
        $language_id = $this->langIdFromPrefix($lang_prefix);
        return PostTranslation::where(compact('post_id', 'language_id'))->get();
    }

    public function insertOrUpdate(string $lang_prefix, array $details): PostTranslation
    {
        Post::firstOrNew(['id' => $details['post_id']]);
        $details['language_id'] = $this->langIdFromPrefix($lang_prefix);
        $search_keys = Arr::only($details, ['post_id', 'language_id']);

        return PostTranslation::updateOrCreate($search_keys, $details);
    }

    public function deletePost(string $lang_prefix, int $post_id): void
    {
        $language_id = $this->langIdFromPrefix($lang_prefix);

        PostTranslation::where(
            compact('post_id', 'language_id')
        )->delete();

    }

    private function getPostByIdAndLang(int $post_id, string $lang_prefix): PostTranslation
    {
        Post::findOrFail($post_id);
        $language_id = $this->langIdFromPrefix($lang_prefix);
        return PostTranslation::wherePostId($post_id)->whereLanguageId($language_id)->get();
    }

    public function setTags(string $lang_prefix, int $post_id, array $tags): array
    {
        Post::findOrFail($post_id);
        return (new PostTag)->setTags($post_id, $tags);
    }
}