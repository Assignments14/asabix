<?php

namespace App\Repositories;

use App\Contracts\TagRepositoryInterface;
use App\Models\Tag;
use Illuminate\Database\Eloquent\Collection;

final class TagRepository implements TagRepositoryInterface
{

    public function getAllTags()
    {
        return Tag::paginate();
    }

    public function createTag(array $tagDetails)
    {
        return Tag::create($tagDetails);
    }

    public function tagById(int $id)
    {
        return Tag::findOrFail($id);
    }

    public function updateTag(int $tagId, array $newDetails)
    {
        return Tag::whereId($tagId)->update($newDetails);
    }

    public function deleteTag(int $tagId)
    {
        Tag::destroy($tagId);
    }
}